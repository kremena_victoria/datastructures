package kremena.learningprogram;

import java.util.LinkedList;

public class Stack {
    private LinkedList<Integer> stack;

    public Stack() {
        stack = new LinkedList<Integer>();
    }

    public void push(int i){
        stack.push(i);
    }

    public int pop(){
        return stack.pop();
    }

    public int peek(){
        return stack.peek();
    }

    public boolean isEmpty(){
        return stack.isEmpty();
    }

    public int size(){
        return stack.size();
    }
}
